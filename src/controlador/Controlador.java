package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Bomba;
import modelo.Gasolina;
import vista.dlgGasolina;

/**
 *
 * @author Brandon
 */
public class Controlador implements ActionListener{
    private dlgGasolina vista;
    private Bomba bom;
    private Gasolina gasolina;
    private int capActBomba;
    private int contador = 0;
    private float precioVenta = 0.0f;
    
    public Controlador(Bomba bom, dlgGasolina vista){
        this.vista = vista;
        this.bom= bom;
        this.gasolina = gasolina;
        
        vista.btnIniciar.addActionListener(this);
        vista.btnRegistrar.addActionListener(this);
        vista.cbTipoGas.addActionListener(this);
        this.capActBomba = vista.jsInventario.getValue();
    }   
     private void iniciarVista(){
        vista.setTitle("Gasolina PEMEX");
        vista.setSize(780, 520);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) { 
        int con = 0;
        if (e.getSource() == vista.btnIniciar) {  
             if(precioVenta!=0.0f){
                int numBomba = Integer.parseInt(vista.txtNumBom.getText());
                String tipoGasolina = vista.cbTipoGas.getSelectedItem().toString();
                Gasolina gasolina = new Gasolina(numBomba, tipoGasolina, precioVenta);
                bom.iniBomba(numBomba, gasolina);
                // Bloquear los campos de texto 
                vista.txtNumBom.setEnabled(false);
                vista.btnIniciar.setEnabled(false);
                vista.btnRegistrar.setEnabled(true);
             }
             else{
                JOptionPane.showMessageDialog(vista, "No se selecciono la calidad");
                vista.btnRegistrar.setEnabled(false);
             }
        }     
        if (e.getSource() == vista.btnRegistrar){
                try{
                    float cantidad = Float.parseFloat(vista.txtCantidad.getText());
                    if (cantidad > capActBomba) {
                    JOptionPane.showMessageDialog(vista, "Gasolina insuficiente.");
                    return; 
                    }

                    float costo = bom.venderGas(cantidad); 
                    if (costo > 0) {
                        float ventasTotales = bom.ventasTotal();
                        vista.txtCosto.setText(String.valueOf(costo));
                        vista.txtTotalVenta.setText(String.valueOf(ventasTotales));

                        int capacidadBomba = vista.jsInventario.getValue();

                        if (capacidadBomba == 0) {
                            JOptionPane.showMessageDialog(vista, "Gasolina insufuciente");
                        } else {
                            capacidadBomba = capActBomba - (int) cantidad;
                            vista.jsInventario.setValue(capacidadBomba);
                            capActBomba = capacidadBomba;

                            // Incrementar el contador de ventas
                            contador=contador+Integer.parseInt(vista.txtCantidad.getText());
                            vista.txtContador.setText(String.valueOf(contador));
                        }
                    }
                    else {
                        JOptionPane.showMessageDialog(vista, "La cantidad que usted ingreso esta fuera de rango");
                    }
                }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
                } catch (Exception ex2){
                     JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
                }   
        }
        else if(e.getSource() == vista.cbTipoGas){
                try{
                    switch(this.vista.cbTipoGas.getSelectedIndex()){
                        case 0:
                            precioVenta = 25.2f;
                            vista.txtPrecioVenta.setText(String.valueOf(precioVenta)); 
                            break;
                        case 1:
                            precioVenta = 27.4f;
                            vista.txtPrecioVenta.setText(String.valueOf(precioVenta));
                            break;
                        case 2:
                            precioVenta = 30.2f;
                            vista.txtPrecioVenta.setText(String.valueOf(precioVenta));
                            break;
                            
                    }
                }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
                } catch (Exception ex2){
                     JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
                } 
        }

    }
         
    public static void main(String[] args) { 
        Bomba bom = new Bomba();
        dlgGasolina vista = new dlgGasolina(new JFrame(), true);
        Controlador contra = new Controlador(bom, vista);
        contra.iniciarVista();
    }

}
