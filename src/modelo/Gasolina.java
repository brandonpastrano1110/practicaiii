package modelo;

public class Gasolina {
    int idGasolina;
    String tipo;
    float precio;
    float cantidad;
    public Gasolina(){
    }
    public Gasolina(Gasolina gas){
        this.idGasolina=gas.idGasolina;
        this.precio=gas.precio;
        this.tipo=gas.tipo;
        this.cantidad=gas.cantidad;
    }
    public Gasolina(int idGasolina, String tipo, float precio){
        this.idGasolina=idGasolina;
        this.tipo=tipo;
        this.precio=precio;
        this.cantidad=200;
    }
    public void setIdGas(int idG){
        this.idGasolina=idG;
    }
    public void setTipo(String ti){
        this.tipo=ti;
    }
    public void setPrecio(float pre){
        this.precio=pre;
    }
    public int getIdGas(){
        return this.idGasolina;
    }
    public String getTipo(){
        return this.tipo;
    }
    public float getPrecio(){
        return this.precio;
    }
    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    
    public float getCantidad() {
        return cantidad;
    }
}
