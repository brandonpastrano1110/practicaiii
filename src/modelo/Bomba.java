package modelo;

/**
 *
 * @author Brandon
 */
public class Bomba extends Gasolina{
    Gasolina gasolina;
    int numBomba;
    float ventasTotales;
    
    public Bomba(){
        this.gasolina=null;
        this.numBomba=0;
        this.ventasTotales=0.0f;
    }
    public Bomba(Bomba bom){
        this.numBomba=bom.numBomba;
        this.gasolina.tipo=bom.gasolina.tipo;
        this.gasolina.precio=bom.gasolina.precio;
        this.ventasTotales=bom.ventasTotales;
    }
    public Bomba(Gasolina gasolina, int numBomba, float ventasTotales){
        this.gasolina=gasolina;
        this.numBomba=numBomba;
        this.ventasTotales=ventasTotales;
    }
    public void setNumBomba(int num){
        this.numBomba=num;
    }
    public void setGasolina(Gasolina gas){
        this.gasolina=gas;
    }
    public int getNumBomba(){
        return this.numBomba;
    }
    public Gasolina getGasolina(){
        return gasolina;
    }
    public void iniBomba(int numB, Gasolina gas){
        this.numBomba=numB;
        this.gasolina=gas;
        this.ventasTotales=0;
    }
    public float venderGas(float can){
        System.out.println("can"+gasolina.getCantidad());
         if (can <= gasolina.getCantidad()) {
            float costo = can * gasolina.getPrecio(); 
            gasolina.setCantidad(gasolina.getCantidad() - can);
            ventasTotales+=costo; 
            return costo;
        }
        else{ 
            return 0;
        }

    }
    public float ventasTotal(){
        return this.ventasTotales;
    }
}
